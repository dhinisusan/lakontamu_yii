<?php

namespace app\models;

use Yii;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

class User extends ActiveRecord implements IdentityInterface
{

    /**
     * @var int Inactive status
     */
    const STATUS_INACTIVE = 0;
    /**
     * @var int Active status
     */
    const STATUS_ACTIVE = 10;

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        // TODO: Implement getId() method.
        // return $this->primaryKey(); //TODO:这里一定要注意了， 返回的是数组而且是字段名称，而上面需要的是字符串。
        return $this->id; //TODO:用这个就OK了。
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * find and set password for user ny id
     */

    public function findPassword($id)
    {
        return true;
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    // public function getImageurl()
    // {
    //     return \Yii::$app->request->BaseUrl . '/' . $this->avatar;
    // }

    public function beforeSave($insert)
    {

        if ($this->isNewRecord) {
            if (isset($this->password_hash)) {
                $this->password_hash = Yii::$app->security->generatePasswordHash($this->password_hash);
            }

        }
        $this->auth_key = Yii::$app->security->generateRandomString();
        return parent::beforeSave($insert);
    }
}
