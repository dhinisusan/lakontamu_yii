<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tb_pegawai".
 *
 * @property string $nip
 * @property string $tempat_lahir
 * @property int $tgl_lahir
 * @property string $nama
 * @property string $gol_ruang
 * @property string $kode_unit
 * @property string $nama_unit
 * @property string $kota
 */
class TbPegawai extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tb_pegawai';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nip'], 'required'],
            [['tgl_lahir', 'kode_unit'], 'integer'],
            [['nip'], 'string', 'max' => 18],
            [['tempat_lahir'], 'string', 'max' => 47],
            [['nama'], 'string', 'max' => 44],
            [['gol_ruang'], 'string', 'max' => 5],
            [['nama_unit'], 'string', 'max' => 112],
            [['kota'], 'string', 'max' => 14],
            [['nip'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()



    
    {
        return [
            'nip' => 'Nip',
            'tempat_lahir' => 'Tempat Lahir',
            'tgl_lahir' => 'Tgl Lahir',
            'nama' => 'Nama',
            'gol_ruang' => 'Gol Ruang',
            'kode_unit' => 'Kode Unit',
            'nama_unit' => 'Nama Unit',
            'kota' => 'Kota',
        ];
    }
}
