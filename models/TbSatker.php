<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tb_satker".
 *
 * @property string $kode_satker
 * @property string $kode_lokasi
 * @property string $nama_satker
 */
class TbSatker extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tb_satker';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kode_satker', 'kode_lokasi', 'nama_satker'], 'required'],
            [['kode_satker', 'kode_lokasi'], 'string', 'max' => 20],
            [['nama_satker'], 'string', 'max' => 300],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kode_satker' => 'Kode Satker',
            'kode_lokasi' => 'Kode Lokasi',
            'nama_satker' => 'Nama Satker',
        ];
    }
}
