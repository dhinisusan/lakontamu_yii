<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tb_lokasi".
 *
 * @property string $kode_lokasi
 * @property string $nama_lokasi
 */
class TbLokasi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tb_lokasi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kode_lokasi', 'nama_lokasi'], 'required'],
            [['kode_lokasi'], 'string', 'max' => 20],
            [['nama_lokasi'], 'string', 'max' => 300],
            [['kode_lokasi'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kode_lokasi' => 'Kode Lokasi',
            'nama_lokasi' => 'Nama Lokasi',
        ];
    }
}
