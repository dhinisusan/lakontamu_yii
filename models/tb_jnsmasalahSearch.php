<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TbJnsmasalah;

/**
 * tb_jnsmasalahSearch represents the model behind the search form of `app\models\TbJnsmasalah`.
 */
class tb_jnsmasalahSearch extends TbJnsmasalah
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kode_jnsmasalah', 'nama_masalah'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TbJnsmasalah::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere(['like', 'kode_jnsmasalah', $this->kode_jnsmasalah])
            ->andFilterWhere(['like', 'nama_masalah', $this->nama_masalah]);

        return $dataProvider;
    }
}
