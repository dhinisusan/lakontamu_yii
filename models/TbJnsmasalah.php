<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tb_jnsmasalah".
 *
 * @property string $kode_jnsmasalah
 * @property string $nama_masalah
 */
class TbJnsmasalah extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tb_jnsmasalah';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kode_jnsmasalah', 'nama_masalah'], 'required'],
            [['kode_jnsmasalah'], 'string', 'max' => 20],
            [['nama_masalah'], 'string', 'max' => 300],
            [['kode_jnsmasalah'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kode_jnsmasalah' => 'Kode Jnsmasalah',
            'nama_masalah' => 'Nama Masalah',
        ];
    }
}
