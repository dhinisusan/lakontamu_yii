<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Tbsatker */

$this->title = 'Update Tbsatker: ' . $model->kode_satker;
$this->params['breadcrumbs'][] = ['label' => 'Tbsatkers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->kode_satker, 'url' => ['view', 'id' => $model->kode_satker]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tbsatker-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
