<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Tbsatker */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbsatker-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'kode_satker')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'kode_lokasi')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nama_satker')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
