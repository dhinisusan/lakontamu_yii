<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\satkerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tbsatkers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbsatker-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Tbsatker', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'kode_satker',
            'kode_lokasi',
            'nama_satker',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
