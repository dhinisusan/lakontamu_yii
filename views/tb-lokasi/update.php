<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TbLokasi */

$this->title = 'Update Tb Lokasi: ' . $model->kode_lokasi;
$this->params['breadcrumbs'][] = ['label' => 'Tb Lokasis', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->kode_lokasi, 'url' => ['view', 'id' => $model->kode_lokasi]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tb-lokasi-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
