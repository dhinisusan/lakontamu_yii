<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TbLokasiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tb Lokasis';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tb-lokasi-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Tb Lokasi', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'kode_lokasi',
            'nama_lokasi',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
