<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TbLokasi */

$this->title = 'Create Tb Lokasi';
$this->params['breadcrumbs'][] = ['label' => 'Tb Lokasis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tb-lokasi-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
