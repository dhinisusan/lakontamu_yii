<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TbJnsmasalah */

$this->title = 'Create Tb Jnsmasalah';
$this->params['breadcrumbs'][] = ['label' => 'Tb Jnsmasalahs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tb-jnsmasalah-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
