<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TbJnsmasalah */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tb-jnsmasalah-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'kode_jnsmasalah')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nama_masalah')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
