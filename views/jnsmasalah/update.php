<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TbJnsmasalah */

$this->title = 'Update Tb Jnsmasalah: ' . $model->kode_jnsmasalah;
$this->params['breadcrumbs'][] = ['label' => 'Tb Jnsmasalahs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->kode_jnsmasalah, 'url' => ['view', 'id' => $model->kode_jnsmasalah]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tb-jnsmasalah-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
