<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\pegawaiSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tb-pegawai-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'nip') ?>

    <?= $form->field($model, 'tempat_lahir') ?>

    <?= $form->field($model, 'tgl_lahir') ?>

    <?= $form->field($model, 'nama') ?>

    <?= $form->field($model, 'gol_ruang') ?>

    <?= $form->field($model, 'kode_unit') ?>

    <?= $form->field($model, 'nama_unit') ?>

    <?= $form->field($model, 'kota') ?>

    <?php // echo $form->field($model, 'peran') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


