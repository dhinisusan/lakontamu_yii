<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\pegawaiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tb Pegawais';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tb-pegawai-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Tb Pegawai', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'nip',
            'tempat_lahir',
            'tgl_lahir',
            'nama',
            'gol_ruang',
            'kode_unit',
            'nama_unit',
            'kota',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
